import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { Observable } from 'rxjs/Rx';
import { TwilioService } from './services/twilio.service';


@Component({
  selector: 'app-twilio',
  templateUrl: './twilio.component.html',
  styleUrls: ['./twilio.component.css']
})
export class TwilioComponent implements OnInit {
  token = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlLTE1OTk3MjAxMzIiLCJpc3MiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlIiwic3ViIjoiQUNlNjU3ZDAyMTRhN2MwYjFiOTI0Yjk1NTkwZTJiZGY4MCIsImV4cCI6MTU5OTcyMzczMiwiZ3JhbnRzIjp7ImlkZW50aXR5Ijoic2FpZiIsInZpZGVvIjp7fX19.5qd7m5BQWPQehZ8nxjCoT9NDDCAouJXDGwI03qWtMOY`;
  message: string;
  accessToken: string;
  roomName: string;
  username: string;

  @ViewChild('localVideo') localVideo: ElementRef;
  @ViewChild('remoteVideo') remoteVideo: ElementRef;

  constructor(private twilioService: TwilioService) {
    this.twilioService.msgSubject.subscribe(r => {
      this.message = r;
    });
  }


  ngOnInit() {
    this.twilioService.localVideo = this.localVideo;
    this.twilioService.remoteVideo = this.remoteVideo;
  //   navigator.mediaDevices.getUserMedia({
  //     'video': true,
  //     'audio': true
  //   }).then(stream => {
  //     console.log('Got MediaStream:', stream);
  // })
  // .catch(error => {
  //     console.error('Error accessing media devices.', error);
  // });
  

      // this.getConnectedDevices('videoinput', cameras => console.log('Cameras found', cameras));
      // this.getConnectedDevices('audiooutput', cameras => console.log('Cameras found', cameras));
      // this.getConnectedDevices('audioinput', cameras => console.log('Cameras found', cameras));
  }

  // getConnectedDevices(type, callback) {
  //   navigator.mediaDevices.enumerateDevices()
  //       .then(devices => {
  //           console.log('devices', devices);
  //           const filtered = devices.filter(device => device.kind === type);
  //           callback(filtered);
  //       });
  //   }

  log(message) {
    this.message = message;
  }

  disconnect() {
    if (this.twilioService.roomObj && this.twilioService.roomObj !== null) {
      this.twilioService.roomObj.disconnect();
      this.twilioService.roomObj = null;
    }
  }

  connect(): void {
    // let storage = JSON.parse(localStorage.getItem('token') || '{}');
    // let date = Date.now();
    if (!this.roomName || !this.username) { this.message = "enter username and room name."; return;}
    console.log('room naem', this.roomName);
    this.accessToken = `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlLTE2MDAxMDUyOTkiLCJpc3MiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlIiwic3ViIjoiQUNlNjU3ZDAyMTRhN2MwYjFiOTI0Yjk1NTkwZTJiZGY4MCIsImV4cCI6MTYwMDEwODg5OSwiZ3JhbnRzIjp7ImlkZW50aXR5IjoiYWJqYXlvbiIsInZpZGVvIjp7fX19.XbpxRU_itkDy49ODodq2MfeBNxhFNIp7xmPIu5qTI8M`;
    this.twilioService.connectToRoom(this.accessToken, { name: this.roomName, audio: true, video: { width: 240 },  networkQuality: true })
    // if (storage['token'] && storage['created_at'] + 3600000 > date) {
    //   this.accessToken = storage['token'];
    //   this.twilioService.connectToRoom(this.accessToken, { name: this.roomName, audio: true, video: { width: 240 },  networkQuality: true })
    //   return;
    // }
    // this.twilioService.getToken(this.username).subscribe(d => {
    //   this.accessToken = d['token'];
    //   // localStorage.setItem('token', JSON.stringify({
    //   //   token: this.accessToken,
    //   //   created_at: date
    //   // }));
    //   console.log('tokennnnnnnnn', this.accessToken);
    //   this.twilioService.connectToRoom(this.accessToken, { name: this.roomName, audio: true, video: { width: 240 },  networkQuality: true })
    // },
    //   error => this.log(JSON.stringify(error)));

  }

}
