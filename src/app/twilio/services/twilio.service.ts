import { Injectable, EventEmitter, ElementRef } from '@angular/core';
import { HttpClientModule, HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { Observer } from 'rxjs';
import { connect, createLocalTracks, createLocalVideoTrack } from 'twilio-video';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import { of } from 'rxjs/observable/of';


@Injectable()
export class TwilioService {
  remoteVideo: ElementRef;
  localVideo: ElementRef;
  previewing: boolean;
  msgSubject = new BehaviorSubject("");
  roomObj: any;

  constructor(private http: HttpClient) {}

  getToken(username): Observable<any> {
    return of({
      token: `eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiIsImN0eSI6InR3aWxpby1mcGE7dj0xIn0.eyJqdGkiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlLTE2MDAxNDc3MTciLCJpc3MiOiJTS2I0Y2JmZDZiMmIxNGE1Njc4NDY1OTcxNWNmZjZkN2VlIiwic3ViIjoiQUNlNjU3ZDAyMTRhN2MwYjFiOTI0Yjk1NTkwZTJiZGY4MCIsImV4cCI6MTYwMDE1MTMxNywiZ3JhbnRzIjp7ImlkZW50aXR5IjoiYWJqYXlvbjIiLCJ2aWRlbyI6e319fQ.bapdUCuQ3Ygrrf27CyKdPrN4qWl8ydNsDMkh4e5GHgM`,
      created_at: new Date().toLocaleString()
    });
  }

  connectToRoom(accessToken: string, options): void {
    console.log('access token',accessToken);
    this.http.get(`http://10.200.96.169:8028/scheduling/v1/programmable-video/token?room=123&identity=${options['name']}`).subscribe(
    (token) => {
      console.log('token');
      connect(token['token'], options).then(room => {
        console.log('successfully connected');
        this.roomObj = room;
        console.log('speed test',room.localParticipant);
        if (!this.previewing && options['video']) {
          this.startLocalVideo();
          this.previewing = true;
        }
  
        // room.participants.forEach(participant => {
        //   this.msgSubject.next("Already in Room: '" + participant.identity + "'");
        //   console.log("Already in Room: '" + participant.identity + "'");
        //   this.attachParticipantTracks(participant);
        // });
        console.log('after local video', room);
        room.on('participantDisconnected', (participant) => {
          this.msgSubject.next("Participant '" + participant.identity + "' left the room");
          console.log("Participant '" + participant.identity + "' left the room");
  
          this.detachParticipantTracks(participant);
        });
  
        room.on('participantConnected',  (participant) => {
          console.log('participant connected', participant);
          // participant.tracks.forEach(track => {
          //   this.remoteVideo.nativeElement.appendChild(track.attach());
          // });
  
          participant.tracks.forEach(publication => {
            if (publication.isSubscribed) {
              const track = publication.track;
              this.remoteVideo.nativeElement.appendChild(track.attach());
            }
          });
  
          participant.on('trackSubscribed', track => {
            this.remoteVideo.nativeElement.appendChild(track.attach());
          });
  
          participant.on('trackAdded', track => {
            console.log('track added')
            this.remoteVideo.nativeElement.appendChild(track.attach());
          });
        });

        room.participants.forEach(participant => {
          participant.tracks.forEach(publication => {
            if (publication.track) {
              this.remoteVideo.nativeElement.appendChild(publication.track.attach());
            }
          });
        
         participant.on('trackSubscribed', track => {
          this.remoteVideo.nativeElement.appendChild(track.attach());
          });
        });
  
        
  
        // When a Participant adds a Track, attach it to the DOM.
        // room.on('trackAdded', (track, participant) => {
        //   console.log(participant.identity + " added track: " + track.kind);
        //   this.attachTracks([track]);
        // });
  
        // When a Participant removes a Track, detach it from the DOM.
        // room.on('trackRemoved', (track, participant) => {
        //   console.log(participant.identity + " removed track: " + track.kind);
        //   this.detachTracks([track]);
        // });
  
        // room.once('disconnected',  room => {
        //   this.msgSubject.next('You left the Room:' + room.name);
        //   room.localParticipant.tracks.forEach(track => {
        //     var attachedElements = track.detach();
        //     attachedElements.forEach(element => element.remove());
        //   });
        // });
      });
    })
   
  }

  attachParticipantTracks(participant): void {
    var tracks = Array.from(participant.tracks.values());
    this.attachTracks([tracks]);
  }

  attachTracks(tracks) {
    tracks.forEach(track => {
      this.remoteVideo.nativeElement.appendChild(track.attach());
    });
  }

  startLocalVideo(): void {
    createLocalVideoTrack().then(track => {
      this.localVideo.nativeElement.appendChild(track.attach());
    });
  }

  localPreview(): void {
    createLocalVideoTrack().then(track => {
      this.localVideo.nativeElement.appendChild(track.attach());
    });
  }

  detachParticipantTracks(participant) {
    var tracks = Array.from(participant.tracks.values());
    this.detachTracks(tracks);
  }

  detachTracks(tracks): void {
    tracks.forEach(function (track) {
      track.detach().forEach(function (detachedElement) {
        detachedElement.remove();
      });
    });
  }

}
